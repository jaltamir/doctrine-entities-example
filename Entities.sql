-- Build all entities without any relation:

CREATE TABLE category (
    id INT AUTO_INCREMENT NOT NULL,
    name VARCHAR(255) NOT NULL,
    slug VARCHAR(255) NOT NULL,
    status SMALLINT NOT NULL,
    created_at DATETIME NOT NULL,
    updated_at DATETIME NOT NULL,
    deleted_at DATETIME DEFAULT NULL,
    UNIQUE INDEX UNIQ_64C19C1989D9B62 (slug),
    PRIMARY KEY (id)
) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB;
CREATE TABLE landing (
    id INT AUTO_INCREMENT NOT NULL,
    name VARCHAR(255) NOT NULL,
    slug VARCHAR(255) NOT NULL,
    status SMALLINT NOT NULL,
    position SMALLINT NOT NULL,
    latitude DOUBLE PRECISION NOT NULL,
    longitude DOUBLE PRECISION NOT NULL,
    application SMALLINT NOT NULL,
    created_at DATETIME NOT NULL,
    updated_at DATETIME NOT NULL,
    deleted_at DATETIME DEFAULT NULL,
    UNIQUE INDEX UNIQ_EF3ACE15989D9B62 (slug),
    PRIMARY KEY (id)
) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB;
CREATE TABLE city (
    id INT AUTO_INCREMENT NOT NULL,
    name VARCHAR(255) NOT NULL,
    slug VARCHAR(255) NOT NULL,
    status SMALLINT NOT NULL,
    position SMALLINT NOT NULL,
    latitude DOUBLE PRECISION NOT NULL,
    longitude DOUBLE PRECISION NOT NULL,
    vat_tax DOUBLE PRECISION NOT NULL,
    content LONGTEXT DEFAULT NULL,
    created_at DATETIME NOT NULL,
    updated_at DATETIME NOT NULL,
    deleted_at DATETIME DEFAULT NULL,
    UNIQUE INDEX UNIQ_2D5B0234989D9B62 (slug),
    PRIMARY KEY (id)
) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB;
CREATE TABLE country (
    id INT AUTO_INCREMENT NOT NULL,
    name VARCHAR(255) NOT NULL,
    slug VARCHAR(255) NOT NULL,
    status SMALLINT NOT NULL,
    created_at DATETIME NOT NULL,
    updated_at DATETIME NOT NULL,
    deleted_at DATETIME DEFAULT NULL,
    UNIQUE INDEX UNIQ_5373C966989D9B62 (slug),
    PRIMARY KEY (id)
) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB;


-- Build ManyToMany relation inside Landing to Category:

CREATE TABLE landing_category (
    landing_id INT NOT NULL,
    category_id INT NOT NULL,
    INDEX IDX_4AF65DD1EFD98736 (landing_id),
    INDEX IDX_4AF65DD112469DE2 (category_id),
    PRIMARY KEY (landing_id, category_id)
) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB;
ALTER TABLE landing_category ADD CONSTRAINT FK_4AF65DD1EFD98736 FOREIGN KEY (landing_id) REFERENCES landing (id) ON DELETE CASCADE;
ALTER TABLE landing_category ADD CONSTRAINT FK_4AF65DD112469DE2 FOREIGN KEY (category_id) REFERENCES category (id) ON DELETE CASCADE;


-- Build ManyToOne relation inside Category to City:

ALTER TABLE category ADD city_id INT DEFAULT NULL;
ALTER TABLE category ADD CONSTRAINT FK_64C19C18BAC62AF FOREIGN KEY (city_id) REFERENCES city (id);
CREATE INDEX IDX_64C19C18BAC62AF ON category (city_id);


-- Build ManyToOne relation inside City to Country:

ALTER TABLE city ADD country_id INT DEFAULT NULL;
ALTER TABLE city ADD CONSTRAINT FK_2D5B0234F92F3E70 FOREIGN KEY (country_id) REFERENCES country (id);
CREATE INDEX IDX_2D5B0234F92F3E70 ON city (country_id);