<?php

namespace AppBundle\Interfaces;

interface Slugable
{
    public function setSlug($slug);

    public function getSlug();
}