<?php

namespace AppBundle\Interfaces;

interface Nameable
{
    public function setName($name);

    public function getName();

    public function __toString();
}