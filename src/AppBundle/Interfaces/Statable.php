<?php

namespace AppBundle\Interfaces;

interface Statable
{
    public function setStatus($status);

    public function getStatus();
}