<?php

namespace AppBundle\Interfaces;

interface Coordinatable
{
    public function setLatitude($latitude);

    public function getLatitude();

    public function setLongitude($longitude);

    public function getLongitude();
}