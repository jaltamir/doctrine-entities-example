<?php

namespace AppBundle\Repository;

use AppBundle\Entity\Category;
use AppBundle\Entity\City;
use AppBundle\Entity\Country;
use Doctrine\ORM\Query\Expr\Join;

class LandingRepository extends \Doctrine\ORM\EntityRepository
{
    public function findLandingsByCountry(Country $country)
    {
        $qb = $this->createQueryBuilder('Landing');
        $qb->join('AppBundle:Category', 'Category', Join::WITH, $qb->expr()->isMemberOf('Category', 'Landing.categories'));
        $qb->join('AppBundle:City', 'City', Join::WITH, $qb->expr()->eq('Category.city', 'City'));
        $qb->join('AppBundle:Country', 'Country', Join::WITH, $qb->expr()->eq('City.country', 'Country'));
        $qb->where($qb->expr()->eq('Country', ':Country'));
        $qb->setParameter('Country', $country);

        return $qb->getQuery()->getResult();
    }

    public function findLandingsByCity(City $city)
    {
        $qb = $this->createQueryBuilder('Landing');
        $qb->join('AppBundle:Category', 'Category', Join::WITH, $qb->expr()->isMemberOf('Category', 'Landing.categories'));
        $qb->join('AppBundle:City', 'City', Join::WITH, $qb->expr()->eq('Category.city', 'City'));
        $qb->where($qb->expr()->eq('City', ':City'));
        $qb->setParameter('City', $city);

        return $qb->getQuery()->getResult();
    }

    public function findLandingsByCategory(Category $category)
    {
        $qb = $this->createQueryBuilder('Landing');
        $qb->join('AppBundle:Category', 'Category', Join::WITH, $qb->expr()->isMemberOf('Category', 'Landing.categories'));
        $qb->where($qb->expr()->eq('Category', ':Category'));
        $qb->setParameter('Category', $category);

        return $qb->getQuery()->getResult();
    }
}
