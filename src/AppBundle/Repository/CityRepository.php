<?php

namespace AppBundle\Repository;

use AppBundle\Entity\Category;
use AppBundle\Entity\Country;
use AppBundle\Entity\Landing;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr\Join;

class CityRepository extends EntityRepository
{
    public function findCitiesByCountry(Country $country)
    {
        $qb = $this->createQueryBuilder('City');
        $qb->join('AppBundle:Country', 'Country', Join::WITH, $qb->expr()->eq('City.country', 'Country'));
        $qb->where($qb->expr()->eq('Country', ':Country'));
        $qb->setParameter('Country', $country);

        return $qb->getQuery()->getResult();
    }

    public function findCitiesByCategory(Category $category)
    {
        $qb = $this->createQueryBuilder('City');
        $qb->join('AppBundle:Category', 'Category', Join::WITH, $qb->expr()->eq('Category.city', 'City'));
        $qb->where($qb->expr()->eq('Category', ':Category'));
        $qb->setParameter('Category', $category);

        return $qb->getQuery()->getResult();
    }

    public function findCitiesByLanding(Landing $landing)
    {
        $qb = $this->createQueryBuilder('City');
        $qb->join('AppBundle:Category', 'Category', Join::WITH, $qb->expr()->eq('Category.city', 'City'));
        $qb->join('AppBundle:Landing', 'Landing', Join::WITH, $qb->expr()->isMemberOf('Category', 'Landing.categories'));
        $qb->where($qb->expr()->eq('Landing', ':Landing'));
        $qb->setParameter('Landing', $landing);

        return $qb->getQuery()->getResult();
    }
}
