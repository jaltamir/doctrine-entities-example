<?php

namespace AppBundle\Repository;

use AppBundle\Entity\City;
use AppBundle\Entity\Country;
use AppBundle\Entity\Landing;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr\Join;

class CategoryRepository extends EntityRepository
{
    public function findCategoriesByCountry(Country $country)
    {
        $qb = $this->createQueryBuilder('Category');
        $qb->join('AppBundle:City', 'City', Join::WITH, $qb->expr()->eq('Category.city', 'City'));
        $qb->join('AppBundle:Country', 'Country', Join::WITH, $qb->expr()->eq('City.country', 'Country'));
        $qb->where($qb->expr()->eq('Country', ':Country'));
        $qb->setParameter('Country', $country);

        return $qb->getQuery()->getResult();
    }

    public function findCategoriesByCity(City $city)
    {
        $qb = $this->createQueryBuilder('Category');
        $qb->join('AppBundle:City', 'City', Join::WITH, $qb->expr()->eq('Category.city', 'City'));
        $qb->where($qb->expr()->eq('City', ':City'));
        $qb->setParameter('City', $city);

        return $qb->getQuery()->getResult();
    }

    public function findCategoriesByLanding(Landing $landing)
    {
        $qb = $this->createQueryBuilder('Category');
        $qb->join('AppBundle:Landing', 'Landing', Join::WITH, $qb->expr()->isMemberOf('Category', 'Landing.categories'));
        $qb->andWhere($qb->expr()->eq('Landing', ':Landing'));
        $qb->setParameter('Landing', $landing);

        return $qb->getQuery()->getResult();
    }
}
