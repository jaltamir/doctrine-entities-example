<?php

namespace AppBundle\Repository;

use AppBundle\Entity\Category;
use AppBundle\Entity\City;
use AppBundle\Entity\Landing;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr\Join;

class CountryRepository extends EntityRepository
{
    public function findCountriesByCity(City $city)
    {
        $qb = $this->createQueryBuilder('Country');
        $qb->join('AppBundle:City', 'City', Join::WITH, $qb->expr()->eq('City.country', 'Country'));
        $qb->where($qb->expr()->eq('City', ':City'));
        $qb->setParameter('City', $city);

        return $qb->getQuery()->getResult();
    }

    public function findCountriesByCategory(Category $category)
    {
        $qb = $this->createQueryBuilder('Country');
        $qb->join('AppBundle:City', 'City', Join::WITH, $qb->expr()->eq('City.country', 'Country'));
        $qb->join('AppBundle:Category', 'Category', Join::WITH, $qb->expr()->eq('Category.city', 'City'));
        $qb->where($qb->expr()->eq('Category', ':Category'));
        $qb->setParameter('Category', $category);

        return $qb->getQuery()->getResult();
    }

    public function findCountriesByLanding(Landing $landing)
    {
        $qb = $this->createQueryBuilder('Country');
        $qb->join('AppBundle:City', 'City', Join::WITH, $qb->expr()->eq('City.country', 'Country'));
        $qb->join('AppBundle:Category', 'Category', Join::WITH, $qb->expr()->eq('Category.city', 'City'));
        $qb->join('AppBundle:Landing', 'Landing', Join::WITH, $qb->expr()->isMemberOf('Category', 'Landing.categories'));
        $qb->where($qb->expr()->eq('Landing', ':Landing'));
        $qb->setParameter('Landing', $landing);

        return $qb->getQuery()->getResult();
    }
}
