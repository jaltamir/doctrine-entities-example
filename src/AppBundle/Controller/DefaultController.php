<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $countries  = $em->getRepository('AppBundle:Country')->findAll();
        $cities     = $em->getRepository('AppBundle:City')->findAll();
        $landings   = $em->getRepository('AppBundle:Landing')->findAll();
        $categories = $em->getRepository('AppBundle:Category')->findAll();

        return $this->render(
            'default/index.html.twig',
            [
                'base_dir' => realpath($this->getParameter('kernel.root_dir') . '/..'),
                'content'  => [
                    'Categories' => $this->findCategories($countries, $cities, $landings),
                    'Cities'     => $this->findCities($countries, $landings, $categories),
                    'Counties'   => $this->findCountries($cities, $landings, $categories),
                    'Landings'   => $this->findLandings($countries, $cities, $categories),
                ],
            ]
        );
    }

    private function findCategories($countries, $cities, $landings)
    {
        $em     = $this->getDoctrine()->getManager();
        $result = [];

        foreach ($countries as $country)
        {
            $categories = $em->getRepository('AppBundle:Category')->findCategoriesByCountry($country);

            foreach ($categories as $category)
            {
                $result['findCategoriesByCountry'][(string) $country][] = (string) $category;
            }
        }

        foreach ($cities as $city)
        {
            $categories = $em->getRepository('AppBundle:Category')->findCategoriesByCity($city);

            foreach ($categories as $category)
            {
                $result['findCategoriesByCity'][(string) $city][] = (string) $category;
            }
        }

        foreach ($landings as $landing)
        {
            $categories = $em->getRepository('AppBundle:Category')->findCategoriesByLanding($landing);

            foreach ($categories as $category)
            {
                $result['findCategoriesByLanding'][(string) $landing][] = (string) $category;
            }
        }

        return $result;
    }

    private function findCities($countries, $landings, $categories)
    {
        $em     = $this->getDoctrine()->getManager();
        $result = [];

        foreach ($countries as $country)
        {
            $cities = $em->getRepository('AppBundle:City')->findCitiesByCountry($country);

            foreach ($cities as $city)
            {
                $result['findCitiesByCountry'][(string) $country][] = (string) $city;
            }
        }

        foreach ($landings as $landing)
        {
            $cities = $em->getRepository('AppBundle:City')->findCitiesByLanding($landing);

            foreach ($cities as $city)
            {
                $result['findCitiesByLanding'][(string) $landing][] = (string) $city;
            }
        }

        foreach ($categories as $category)
        {
            $cities = $em->getRepository('AppBundle:City')->findCitiesByCategory($category);

            foreach ($cities as $city)
            {
                $result['findCitiesByCategory'][(string) $category][] = (string) $city;
            }
        }

        return $result;
    }

    private function findCountries($cities, $landings, $categories)
    {
        $em     = $this->getDoctrine()->getManager();
        $result = [];

        foreach ($cities as $city)
        {
            $countries = $em->getRepository('AppBundle:Country')->findCountriesByCity($city);

            foreach ($countries as $country)
            {
                $result['findCountriesByCity'][(string) $city][] = (string) $country;
            }
        }

        foreach ($landings as $landing)
        {
            $countries = $em->getRepository('AppBundle:Country')->findCountriesByLanding($landing);

            foreach ($countries as $country)
            {
                $result['findCountriesByLanding'][(string) $landing][] = (string) $country;
            }
        }

        foreach ($categories as $category)
        {
            $countries = $em->getRepository('AppBundle:Country')->findCountriesByCategory($category);

            foreach ($countries as $country)
            {
                $result['findCountriesByCategory'][(string) $category][] = (string) $country;
            }
        }

        return $result;
    }

    private function findLandings($countries, $cities, $categories)
    {
        $em     = $this->getDoctrine()->getManager();
        $result = [];

        foreach ($countries as $country)
        {
            $landings = $em->getRepository('AppBundle:Landing')->findLandingsByCountry($country);

            foreach ($landings as $landing)
            {
                $result['findLandingsByCountry'][(string) $country][] = (string) $landing;
            }
        }

        foreach ($cities as $city)
        {
            $landings = $em->getRepository('AppBundle:Landing')->findLandingsByCity($city);

            foreach ($landings as $landing)
            {
                $result['findLandingsByCity'][(string) $city][] = (string) $landing;
            }
        }

        foreach ($categories as $category)
        {
            $landings = $em->getRepository('AppBundle:Landing')->findLandingsByCategory($category);

            foreach ($landings as $landing)
            {
                $result['findLandingsByCategory'][(string) $category][] = (string) $landing;
            }
        }

        return $result;
    }
}
