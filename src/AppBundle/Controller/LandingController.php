<?php

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use AppBundle\Entity\Landing;
use AppBundle\Form\LandingType;

/**
 * Landing controller.
 *
 * @Route("/landing")
 */
class LandingController extends Controller
{
    /**
     * Lists all Landing entities.
     *
     * @Route("/", name="landing_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $landings = $em->getRepository('AppBundle:Landing')->findAll();

        return $this->render('landing/index.html.twig', array(
            'landings' => $landings,
        ));
    }

    /**
     * Creates a new Landing entity.
     *
     * @Route("/new", name="landing_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $landing = new Landing();
        $form = $this->createForm('AppBundle\Form\LandingType', $landing);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($landing);
            $em->flush();

            return $this->redirectToRoute('landing_show', array('id' => $landing->getId()));
        }

        return $this->render('landing/new.html.twig', array(
            'landing' => $landing,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Landing entity.
     *
     * @Route("/{id}", name="landing_show")
     * @Method("GET")
     */
    public function showAction(Landing $landing)
    {
        $deleteForm = $this->createDeleteForm($landing);

        return $this->render('landing/show.html.twig', array(
            'landing' => $landing,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Landing entity.
     *
     * @Route("/{id}/edit", name="landing_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Landing $landing)
    {
        $deleteForm = $this->createDeleteForm($landing);
        $editForm = $this->createForm('AppBundle\Form\LandingType', $landing);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($landing);
            $em->flush();

            return $this->redirectToRoute('landing_edit', array('id' => $landing->getId()));
        }

        return $this->render('landing/edit.html.twig', array(
            'landing' => $landing,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a Landing entity.
     *
     * @Route("/{id}", name="landing_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Landing $landing)
    {
        $form = $this->createDeleteForm($landing);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($landing);
            $em->flush();
        }

        return $this->redirectToRoute('landing_index');
    }

    /**
     * Creates a form to delete a Landing entity.
     *
     * @param Landing $landing The Landing entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Landing $landing)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('landing_delete', array('id' => $landing->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
