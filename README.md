Doctrine Entities Example
=========================

A Symfony project created on October 3, 2016, 12:30 pm.

This projects show how to build a powerfully solid Entity schema with
many interfaces that solve a basic problem when put manual field names
or properties that can mutate between entities, instead of use the
same field name or the property when the values are exactly the same.

With using a minimum relations between all entities, you can view how to
get to all possible relations from all entities with any entity related,
with simply using of the proper Repository method, like the old school
with SQL dialects and many JOINs...

Remember: JOINS DON'T HURT YOU MEN!
